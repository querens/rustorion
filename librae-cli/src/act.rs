use anyhow::{anyhow, bail, Context, Result};
use rustorion::action;
use std::fs::File;
use std::io;

#[derive(clap::Parser)]
#[clap(about = "Apply actions to universe")]
pub struct Options {
	empire_name: String,

	actions_file: String,

	#[clap(long)]
	/// Validate the actions first
	validate: bool,
}

pub fn main(options: Options) -> Result<()> {
	let mut u: rustorion::universe::Universe = ciborium::from_reader(io::stdin()).context("Failed to parse universe")?;
	let actor = {
		let ui = u.interface();
		let empire = *ui.empires().iter().find(|e| e.data.name == options.empire_name).ok_or_else(|| anyhow!("Failed to locate empire"))?;
		action::Actor { empire_id: empire.data.id.cast() }
	};
	let file = File::open(&options.actions_file).expect("Failed to open file");
	let actions: Vec<action::Action> = ciborium::from_reader(file).context(format!("Failed to parse action file {}", options.actions_file))?;
	let actions = if options.validate {
		let u_v = u.empire_view(actor.empire_id.cast());
		match action::ActionCache::new_batch(actions, &u_v, &actor) {
			Err(action::ActionsFailed { failures }) => {
				let errors: Vec<_> = failures.into_iter().map(|f| f.error).collect();
				bail!((errors).join("\n"))
			}
			Ok(cache) => cache.actions_vec(),
		}
	} else {
		actions
	};
	for action in actions {
		action.apply(&mut u, &actor);
	}
	ciborium::into_writer(&u, &mut io::stdout())?;
	Ok(())
}
