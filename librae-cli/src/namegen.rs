use anyhow::Result;
use rustorion::names;

#[derive(clap::Parser)]
pub struct Options {
	// word length
	length: usize,
	// suffix
	#[clap(short = 's', default_value = "")]
	suffix: String,
}

pub fn main(options: Options) -> Result<()> {
	let u = names::generate_name(&mut rand::thread_rng(), options.length, &options.suffix, rustorion::names::NameGenerator::Autistic);
	println!("{}", u);
	Ok(())
}
