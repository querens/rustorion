use anyhow::{Context, Result};
use std::io;
use thiserror::Error;

#[derive(Error, Debug)]
enum FindError {
	#[error("Satisfying record not found")]
	NotFound,
}

/// Find an object's ID within a view or universe
#[derive(clap::Parser)]
#[clap(about)]
pub struct Options {
	/// Input is an universe
	#[clap(short = 'u')]
	universe: bool,
	/// What to find
	#[clap(subcommand)]
	selector: Selector,
}

#[derive(clap::Parser)]
pub enum Selector {
	#[clap(subcommand)]
	Empire(EmpireSelector),
	#[clap(subcommand)]
	StarSystem(StarSystemSelector),
}

#[derive(clap::Parser)]
pub enum EmpireSelector {
	Any,
	Name { name: String },
}

#[derive(clap::Parser)]
pub enum StarSystemSelector {
	Any,
	Name { name: String },
}

pub fn main(options: Options) -> Result<()> {
	let u_v = if options.universe {
		let universe = ciborium::from_reader(io::stdin()).context("Failed to parse universe")?;
		rustorion::universeview::Universe::perfect_view(&universe)
	} else {
		ciborium::from_reader(io::stdin()).context("Failed to parse universe")?
	};

	let id = match options.selector {
		Selector::Empire(EmpireSelector::Any) => u_v.empires.values().next().map(|e| e.id).ok_or(FindError::NotFound)?,
		Selector::Empire(EmpireSelector::Name { name }) => u_v.empires.values().find(|e| e.name == name).map(|e| e.id).ok_or(FindError::NotFound)?,
		Selector::StarSystem(StarSystemSelector::Any) => u_v.empires.values().next().map(|ss| ss.id).ok_or(FindError::NotFound)?,
		Selector::StarSystem(StarSystemSelector::Name { name }) => u_v.empires.values().find(|ss| ss.name == name).map(|ss| ss.id).ok_or(FindError::NotFound)?,
	};

	println!("{}", id.0);
	Ok(())
}
