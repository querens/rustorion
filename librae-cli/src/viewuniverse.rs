use rustorion::universeview;
// use rustorion::universe::interface;
use anyhow::{anyhow, Result};
use std::io;

#[derive(clap::Parser)]
#[clap(about = "Generate a view of universe from STDIN")]
pub struct Options {
	#[clap(short = 'e')]
	/// Name of empire for non-perfect view
	empire_id: Option<u64>,
	/// Force perfect view
	#[clap(short = 'p', long = "perfect")]
	perfect: bool,
}

pub fn main(options: Options) -> Result<()> {
	let u: rustorion::universe::Universe = ciborium::from_reader(io::stdin()).expect("Failed to parse universe");
	let ui = u.interface();
	let view = match options.empire_id {
		Some(empire_id) => {
			let empires = ui.empires();
			let target = empires
				.iter()
				.find(|e| e.data.id == rustorion::storage::ID::from_id(empire_id))
				.ok_or_else(|| anyhow!("Failed to locate empire"))?;
			if options.perfect {
				let mut view = universeview::Universe::perfect_view(&u);
				view.controlled_empire = Some(target.data.id.cast());
				view
			} else {
				universeview::Universe::empire_view(&u, target.data.id)
			}
		}
		None => universeview::Universe::perfect_view(&u),
	};
	ciborium::into_writer(&view, io::stdout())?;
	Ok(())
}
