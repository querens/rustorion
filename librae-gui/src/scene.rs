use gtk::gdk;
use gtk::gio;
use gtk::glib;
use gtk::graphene;
use gtk::gsk;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use std::cell::RefCell;
use std::collections::hash_map::HashMap;
use thiserror::Error;

glib::wrapper! {
	pub struct SceneWidget(ObjectSubclass<SceneWidgetImp>)
		@extends gtk::Widget;
}

impl Default for SceneWidget {
	fn default() -> Self {
		Self::new()
	}
}

impl SceneWidget {
	pub fn new() -> Self {
		glib::Object::new()
	}
}

#[derive(Default)]
pub struct SceneWidgetImp {
	scene: RefCell<Option<Scene>>,
}

#[glib::object_subclass]
impl ObjectSubclass for SceneWidgetImp {
	const NAME: &'static str = "SceneWidget";
	type Type = SceneWidget;
	type ParentType = gtk::Widget;
	type Interfaces = ();
}

impl ObjectImpl for SceneWidgetImp {}

impl WidgetImpl for SceneWidgetImp {
	fn snapshot(&self, snapshot: &gtk::Snapshot) {
		let widget = self.obj();
		let mut scene = widget.scene();
		snapshot.scale(scene.zoom as f32, scene.zoom as f32);
		let drawables: std::collections::BTreeSet<_> = scene.drawables.keys().cloned().collect();

		let mut click_tree = rstar::RTree::new();

		for drawable_index in drawables.into_iter() {
			let drawable = scene.drawables.get(&drawable_index).unwrap();
			if let Some((node, drawing_data)) = drawable.draw(&scene) {
				let node = scene.position(node, drawing_data);
				let rect = rstar::primitives::GeomWithData::new(SceneWidget::to_rstar_rect(&node.bounds()), drawable_index);
				click_tree.insert(rect);
				snapshot.append_node(node);
			}
		}
		scene.click_tree = click_tree;
	}
}

impl SceneWidget {
	pub fn scene(&self) -> std::cell::RefMut<Scene> {
		std::cell::RefMut::map(self.imp().scene.borrow_mut(), |s| s.as_mut().unwrap())
	}

	pub fn init(&self, width: u64, height: u64) {
		let scene = Scene {
			drawables: HashMap::new(),
			click_tree: rstar::RTree::new(),
			width,
			height,
			drawable_counter: 0,
			zoom_level: 9,
			zoom: 1.,
			image_cache: Default::default(),
		};

		let clicker = gtk::GestureClick::builder().button(1).name("clicker").build();
		self.add_controller(clicker.clone());

		let self_clone = self.clone();
		clicker.connect_released(move |_controller, _click, x, y| {
			let scene = self_clone.scene();
			let point = (x / scene.zoom, y / scene.zoom);
			for click_region in scene.click_tree.locate_all_at_point(&[point.0, point.1]) {
				let drawable = scene.drawables.get(&click_region.data).unwrap();
				drawable.click(1);
			}
		});

		let clicker3 = gtk::GestureClick::builder().button(3).name("clicker3").build();
		self.add_controller(clicker3.clone());
		let self_clone = self.clone();
		clicker3.connect_released(move |_controller, _click, x, y| {
			let scene = self_clone.scene();
			let point = (x / scene.zoom, y / scene.zoom);
			for click_region in scene.click_tree.locate_all_at_point(&[point.0, point.1]) {
				let drawable = scene.drawables.get(&click_region.data).unwrap();
				drawable.click(3);
			}
		});
		*self.imp().scene.borrow_mut() = Some(scene);
	}

	pub fn add_drawable(&self, drawable: impl Drawable + 'static) -> DrawableIndex {
		let mut scene = self.scene();

		let drawable_index = DrawableIndex(drawable.z_index(), scene.drawable_counter);
		scene.drawable_counter = scene.drawable_counter.checked_add(1).expect("Z-Index map out of capacity");

		scene.drawables.insert(drawable_index, Box::new(drawable));
		self.queue_draw();
		drawable_index
	}

	pub fn to_rstar_rect(rect: &graphene::Rect) -> rstar::primitives::Rectangle<[f64; 2]> {
		rstar::primitives::Rectangle::from_corners(
			[rect.top_left().x() as f64, rect.top_left().y() as f64],
			[rect.bottom_right().x() as f64, rect.bottom_right().y() as f64],
		)
	}

	pub fn remove_drawable(&self, drawable_index: DrawableIndex) {
		let mut scene = self.scene();
		scene.drawables.remove(&drawable_index).unwrap();
		self.queue_draw();
	}

	pub fn update_zoom_level(&self, zoom_level: usize) -> std::result::Result<(), BadZoomLevel> {
		let mut scene = self.scene();
		scene.zoom = scene.level_to_zoom(zoom_level)?;
		scene.zoom_level = zoom_level;

		let zoomed_size = scene.zoomed_size();
		self.set_size_request(zoomed_size[0], zoomed_size[1]);

		self.queue_draw();
		Ok(())
	}

	pub fn zoom(&self) -> f64 {
		self.scene().zoom
	}

	pub fn zoom_level(&self) -> usize {
		self.scene().zoom_level
	}
}

pub struct Scene {
	#[allow(clippy::type_complexity)]
	pub drawables: HashMap<DrawableIndex, Box<dyn Drawable>>,
	pub click_tree: rstar::RTree<rstar::primitives::GeomWithData<rstar::primitives::Rectangle<[f64; 2]>, DrawableIndex>>,
	pub drawable_counter: u64,
	pub width: u64,
	pub height: u64,
	pub zoom_level: usize,
	pub zoom: f64,
	pub image_cache: RefCell<HashMap<&'static str, gtk::IconPaintable>>,
}
#[derive(Error, Debug)]
#[error("Zoom level out of bounds")]
pub struct BadZoomLevel;

impl Scene {
	// make sure size is sane
	pub fn translate_size(&self, size: i64) -> f64 {
		(size as f64).max(1.0)
	}

	// size of widget after scaling
	// i32 for gtk
	pub fn zoomed_size(&self) -> [i32; 2] {
		[(self.width as f64 * self.zoom).ceil() as i32, (self.height as f64 * self.zoom).ceil() as i32]
	}

	// translate human-understandable integral "zoom level" to zoom ratio. Returns BadZoomLevel if that level is not supported
	pub fn level_to_zoom(&self, zoom_level: usize) -> std::result::Result<f64, BadZoomLevel> {
		let zoom_levels = 10;
		if zoom_level > zoom_levels {
			return Err(BadZoomLevel);
		};
		let min_zoom: f64 = (1000. / self.width as f64).max(1000. / self.height as f64).log2();
		let max_zoom: f64 = (min_zoom * 2.0).max((1.0_f64).log2()); // 1000 SMU = 1000 pixels

		let step = (max_zoom - min_zoom) / zoom_levels as f64;
		Ok(2.0_f64.powf(min_zoom + step * zoom_level as f64))
	}

	// draw an image from resources onto given snapshot, scaling it to requested size, with given color
	// image is cached internally
	pub fn render_image(&self, name: &'static str, snapshot: &gtk::Snapshot, size: i32, color: rustorion::color::Color) {
		let mut cache = self.image_cache.borrow_mut();
		let image = cache.entry(name).or_insert_with(|| {
			let path = format!("resources/{}.svg", name); // TODO: flex this
			let file = gio::File::for_path(path);
			gtk::IconPaintable::for_file(&file, 666, 1)
		});
		let color_f: (f32, f32, f32) = color.into();
		snapshot.push_color_matrix(
			&graphene::Matrix::from_float([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]),
			&graphene::Vec4::from_float([color_f.0, color_f.1, color_f.2, 0.]),
		);
		let snapshot_gdk: gdk::Snapshot = snapshot.clone().upcast();
		image.snapshot(&snapshot_gdk, size as f64, size as f64);
		snapshot.pop();
	}

	// transform the node in the way described in drawing_data
	pub fn position(&self, node: gsk::RenderNode, drawing_data: DrawingData) -> gsk::RenderNode {
		// assuming 0.0 is real top left
		let bounds = node.bounds();
		let bounds = bounds.expand(&graphene::Point::new(0., 0.));

		let antizoom = 1.0 / self.zoom;
		let width = bounds.width() as f64;
		let height = bounds.height() as f64;
		let (size, scale): (ScaledCoords, _) = match drawing_data.size {
			DrawableSize::RealSize(size) => {
				let wadj = size.x as f64 / width;
				let hadj = size.y as f64 / height;
				(size.into(), (wadj, hadj))
			}
			DrawableSize::PixelSize(size) => {
				let wadj = size.x / width;
				let hadj = size.y / height;
				((width * wadj * antizoom, height * hadj * antizoom).into(), (wadj * antizoom, hadj * antizoom))
			}
			DrawableSize::Original => ((width, height).into(), (1.0, 1.0)),
			DrawableSize::OriginalPixel => ((width * antizoom, height * antizoom).into(), (antizoom, antizoom)),
		};
		let coords: ScaledCoords = (
			drawing_data.coords.x as f64 + drawing_data.alignment.hoffset(size.x) + drawing_data.offset.x * antizoom,
			drawing_data.coords.y as f64 + drawing_data.alignment.voffset(size.y) + drawing_data.offset.y * antizoom + drawing_data.stack_number as f64 * size.y,
		)
			.into();

		let snapshot = gtk::Snapshot::new();
		snapshot.scale(scale.0 as f32, scale.1 as f32);
		snapshot.append_node(node);
		let node = snapshot.to_node().unwrap();
		let snapshot = gtk::Snapshot::new();
		snapshot.translate(&graphene::Point::new(coords.x as f32, coords.y as f32));
		snapshot.append_node(node);

		snapshot.to_node().unwrap()
	}
}

#[derive(Debug, Clone, Copy)]
pub enum DrawableSize {
	/// resize to real (pre-zoom) size
	RealSize(SceneCoords),
	/// resize to displayed pixels
	PixelSize(ScaledCoords),
	/// keep original size
	Original,
	/// keep original size and compensate zoom
	OriginalPixel,
}

pub trait Drawable {
	fn draw(&self, scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)>;
	fn click(&self, _number: u64) {}

	fn z_index(&self) -> i64 {
		0
	}

	fn drawing_data(&self, scene: &Scene) -> Option<DrawingData> {
		if let Some((node, drawing_data)) = self.draw(scene) {
			let positioned_node = scene.position(node, drawing_data);
			let bounds = positioned_node.bounds();
			Some(DrawingData {
				coords: (bounds.x() as i64, bounds.y() as i64).into(),
				size: DrawableSize::RealSize((bounds.width() as i64, bounds.height() as i64).into()),
				..Default::default()
			})
		} else {
			None
		}
	}
}

pub fn draw_line(from: SceneCoords, to: SceneCoords, color: &gdk::RGBA, line_width: u64) -> gtk::gsk::RenderNode {
	let snapshot = gtk::Snapshot::new();
	let vector = to - from;
	let size = ((vector.x.pow(2) + vector.y.pow(2)) as f32).sqrt();

	let tangent = vector.x as f32 / vector.y as f32;
	let argtan = tangent.atan();
	let angle = if vector.y < 0 { argtan.to_degrees() + 180. } else { argtan.to_degrees() };

	snapshot.translate(&graphene::Point::new(from.x as f32, from.y as f32));
	snapshot.rotate(-angle);
	snapshot.append_color(color, &graphene::Rect::new(0., 0., line_width as f32, size));
	snapshot.to_node().unwrap()
}

#[derive(Clone, Debug)]
pub struct DrawingData {
	pub coords: SceneCoords,
	pub size: DrawableSize,
	pub alignment: Alignment,
	pub offset: ScaledCoords,
	pub stack_number: u64,
}
impl Default for DrawingData {
	fn default() -> Self {
		DrawingData {
			alignment: Alignment {
				horizontal: HAlignment::Left,
				vertical: VAlignment::Top,
			},
			coords: (0, 0).into(),
			size: DrawableSize::Original,
			offset: (0., 0.).into(),
			stack_number: 0,
		}
	}
}

#[derive(Clone, Debug)]
pub struct Alignment {
	pub horizontal: HAlignment,
	pub vertical: VAlignment,
}

impl Alignment {
	pub fn center() -> Alignment {
		Alignment {
			horizontal: HAlignment::Middle,
			vertical: VAlignment::Middle,
		}
	}

	pub fn hoffset(&self, size: f64) -> f64 {
		let multiplier = match self.horizontal {
			HAlignment::Left => 0.,
			HAlignment::Middle => -0.5,
			HAlignment::Right => -1.0,
		};
		multiplier * size
	}

	pub fn voffset(&self, size: f64) -> f64 {
		let multiplier = match self.vertical {
			VAlignment::Top => 0.,
			VAlignment::Middle => -0.5,
			VAlignment::Bottom => -1.0,
		};
		multiplier * size
	}
}

#[derive(Clone, Debug)]
pub enum HAlignment {
	Left,
	Middle,
	Right,
}

#[derive(Clone, Debug)]
pub enum VAlignment {
	Top,
	Middle,
	Bottom,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct ScaledCoords {
	pub x: f64,
	pub y: f64,
}
impl core::ops::Div<f64> for ScaledCoords {
	type Output = Self;

	fn div(self, other: f64) -> Self::Output {
		ScaledCoords { x: self.x / other, y: self.y / other }
	}
}

impl core::ops::Mul<f64> for ScaledCoords {
	type Output = Self;

	fn mul(self, other: f64) -> Self::Output {
		ScaledCoords { x: self.x * other, y: self.y * other }
	}
}

impl core::ops::Sub for ScaledCoords {
	type Output = Self;

	fn sub(self, other: Self) -> Self::Output {
		ScaledCoords {
			x: self.x - other.x,
			y: self.y - other.y,
		}
	}
}

impl core::ops::Add for ScaledCoords {
	type Output = Self;

	fn add(self, other: Self) -> Self::Output {
		ScaledCoords {
			x: self.x + other.x,
			y: self.y + other.y,
		}
	}
}

impl From<(f64, f64)> for ScaledCoords {
	fn from(is: (f64, f64)) -> ScaledCoords {
		ScaledCoords { x: is.0, y: is.1 }
	}
}

// for rstar i think
impl From<ScaledCoords> for [f64; 2] {
	fn from(scaled_coords: ScaledCoords) -> [f64; 2] {
		[scaled_coords.x, scaled_coords.y]
	}
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct SceneCoords {
	pub x: i64,
	pub y: i64,
}

impl core::ops::Sub for SceneCoords {
	type Output = Self;

	fn sub(self, other: Self) -> Self::Output {
		SceneCoords {
			x: self.x - other.x,
			y: self.y - other.y,
		}
	}
}

impl core::ops::Add for SceneCoords {
	type Output = Self;

	fn add(self, other: Self) -> Self::Output {
		SceneCoords {
			x: self.x + other.x,
			y: self.y + other.y,
		}
	}
}

impl core::ops::Mul<i64> for SceneCoords {
	type Output = Self;

	fn mul(self, other: i64) -> Self::Output {
		SceneCoords { x: self.x * other, y: self.y * other }
	}
}

impl From<(i64, i64)> for SceneCoords {
	fn from(is: (i64, i64)) -> SceneCoords {
		SceneCoords { x: is.0, y: is.1 }
	}
}

impl From<SceneCoords> for ScaledCoords {
	fn from(sc: SceneCoords) -> ScaledCoords {
		ScaledCoords { x: sc.x as f64, y: sc.y as f64 }
	}
}

#[derive(PartialEq, Eq, Ord, PartialOrd, Clone, Copy, Hash)]
pub struct DrawableIndex(i64, u64);
