#![cfg_attr(feature = "strict", deny(warnings))]

pub mod action;
pub mod color;
pub mod names;
pub mod net;
pub mod storage;
pub mod units;
pub mod universe;
pub mod universegen;
pub mod universeview;
pub mod util;

use anyhow::{Context, Result};

pub const AFFERO: &str = include_str!("affero.txt");

pub fn project_dir() -> Result<directories::ProjectDirs> {
	directories::ProjectDirs::from("", "rustorion", "rustorion").context("failed to lookup data directory")
}
pub fn ensure_tracing_subscriber() {
	let filter = tracing_subscriber::filter::EnvFilter::builder()
		.with_default_directive(tracing_subscriber::filter::LevelFilter::WARN.into())
		.with_env_var("LOG_LEVEL")
		.from_env()
		.unwrap();
	let _ = tracing_subscriber::fmt().with_writer(std::io::stderr).with_env_filter(filter).try_init();
}
