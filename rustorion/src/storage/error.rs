use crate::storage;
use thiserror::Error;

#[derive(Error, Debug)]
#[error("Entity ID={} was not present in storage", id)]
pub struct EntityMissing {
	pub id: storage::StorageID,
}
