pub use num_traits::ops::checked::*;
pub use num_traits::ops::saturating::*;
pub use num_traits::ToPrimitive;
use serde::{Deserialize, Serialize};
use std::marker::PhantomData;
pub use std::ops::*;
use thiserror::Error;

// this module is meant to be used without prefix, as its names are often used.

pub trait FloatPrintable {
	fn floatprint(self) -> String;
}

impl FloatPrintable for Rational {
	fn floatprint(self) -> String {
		let f_option = self.to_f64();
		if let Some(f) = f_option {
			format!("{:.3}", f)
		} else {
			tracing::warn!("Invalid rational: {:?}", self);
			"n/a".into()
		}
	}
}

pub type Rational = num_rational::Ratio<i128>;

pub fn rerange_rational(value: Rational, original_from: Rational, original_to: Rational, from: Rational, to: Rational) -> Rational {
	assert!(original_from <= original_to);
	assert!(from <= to);
	assert!(value >= original_from);
	assert!(value <= original_to);
	let original_range = original_to.sub(original_from);
	let range = to.sub(from);
	let deoffset_value = value.sub(original_from);
	let reranged_value = deoffset_value.div(original_range).mul(range);
	reranged_value.add(from)
}

pub trait UnitType: Sized + std::fmt::Debug + Clone + Default + Eq + Ord {
	const UNIT_NAME: &'static str;
}

// 1 CosmicRayIntensity = 1 EC planetary insurance cost increase
// 1 CosmicRayIntensity is the normal background in space, added by system star and compensated by EM shielding, caused by either spinning planet core or artificial sources
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct CosmicRayIntensity;

impl UnitType for CosmicRayIntensity {
	const UNIT_NAME: &'static str = "normal";
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct CoreSpinSpeed;

impl UnitType for CoreSpinSpeed {
	const UNIT_NAME: &'static str = "earth";
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct SurfaceBioQuality;

impl UnitType for SurfaceBioQuality {
	const UNIT_NAME: &'static str = "earth";
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct AtmosphereBioQuality;

impl UnitType for AtmosphereBioQuality {
	const UNIT_NAME: &'static str = "earth";
}

// 1 human
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Population;

impl UnitType for Population {
	const UNIT_NAME: &'static str = "human";
}

// amount of government-issued cryptocurrency redeemable for 1 Fuel in its capital
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct EnergyCredit;

impl UnitType for EnergyCredit {
	const UNIT_NAME: &'static str = "ec";
}

// amount of food consumed by a single human, living on a terran planet, in a week, to remain healthy
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Food;

impl UnitType for Food {
	const UNIT_NAME: &'static str = "ration";
}

// amount of fusion fuel 1 organized and basically equipped human living on an average dead-core moon can produce in a week
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Fuel;
impl UnitType for Fuel {
	const UNIT_NAME: &'static str = "fuel";
}

// with 1.0 availability, 1 human equipped with common tools can gather 1 resource in a week
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct ResourceAvailability;
impl UnitType for ResourceAvailability {
	const UNIT_NAME: &'static str = "normal";
}

// Enough farming facilities to produce 1 ration in 1 week in local conditions
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Farm;
impl UnitType for Farm {
	const UNIT_NAME: &'static str = "farm";
}

// Enough fuel sifting facilities to produce 1 EC in 1 week in local conditions
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct FuelSifter;
impl UnitType for FuelSifter {
	const UNIT_NAME: &'static str = "fuel sifter";
}

#[derive(Error, Debug)]
#[error("Units storage overflow")]
pub struct UnitsOverflow;

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct Unit<UnitName: UnitType> {
	pub microunits: i64,
	#[serde(skip)]
	phantom: PhantomData<UnitName>,
}

impl<UnitName: UnitType> num_traits::identities::Zero for Unit<UnitName> {
	fn zero() -> Self {
		0.into()
	}
	fn is_zero(&self) -> bool {
		self.microunits == 0
	}
}

impl<UnitName: UnitType> std::iter::Sum for Unit<UnitName> {
	fn sum<I: Iterator<Item = Unit<UnitName>>>(iter: I) -> Unit<UnitName> {
		iter.fold(0.into(), |a, b| a.add(b))
	}
}

impl<UnitName: UnitType> Copy for Unit<UnitName> {}

impl<UnitName: UnitType> PartialEq for Unit<UnitName> {
	fn eq(&self, other: &Unit<UnitName>) -> bool {
		self.microunits.eq(&other.microunits)
	}
}

impl<UnitName: UnitType> Clone for Unit<UnitName> {
	fn clone(&self) -> Self {
		*self
	}
}

impl<UnitName: UnitType> Eq for Unit<UnitName> {}

impl<UnitName: UnitType> PartialOrd for Unit<UnitName> {
	fn partial_cmp(&self, other: &Unit<UnitName>) -> Option<std::cmp::Ordering> {
		Some(self.cmp(other))
	}
}

impl<UnitName: UnitType> Ord for Unit<UnitName> {
	fn cmp(&self, other: &Unit<UnitName>) -> std::cmp::Ordering {
		self.microunits.cmp(&other.microunits)
	}
}

lazy_static::lazy_static! {
	static ref TRAILING_ZEROES: regex::Regex = regex::Regex::new("0+$").unwrap();
}

pub fn display_parts(microunits: u64) -> String {
	let log = (microunits as f32).log(10.).round() as u64;
	let si_level = log.div_euclid(3);
	let si_prefix = match si_level {
		0 => " mk",
		1 => " m",
		2 => "",
		3 => " K",
		4 => " M",
		5 => " G",
		6 => " T",
		7 => " P",
		8 => " E",
		9 => " Z",
		10 => " Y",
		_ => "",
	};
	let number = format!("{}", microunits);
	let dot = (si_level * 3) as usize;
	let fulls = &number[0..number.len() - dot];
	let fulls = if fulls.is_empty() { "0" } else { fulls };
	let decimals = &number[number.len() - dot..(number.len() + 3 - 1 - dot).min(number.len())];
	let filtered_decimals = TRAILING_ZEROES.replace(decimals, "");
	let number_string = if filtered_decimals.len() == 0 {
		fulls.to_string()
	} else {
		format!("{fulls}.{filtered_decimals}")
	};
	if si_level > 10 {
		format!("{number_string}e{dot}")
	} else {
		format!("{number_string}{si_prefix}")
	}
}

impl<UnitName: UnitType> Unit<UnitName> {
	pub fn from_microunits(microunits: i64) -> Self {
		Unit {
			microunits,
			phantom: Default::default(),
		}
	}
	pub fn quantity(self) -> String {
		let sign = if self.microunits < 0 { "-" } else { "" };
		format!("{sign}{}", display_parts(self.microunits.unsigned_abs()))
	}
}

impl<UnitName: UnitType> From<i64> for Unit<UnitName> {
	fn from(units: i64) -> Self {
		let microunits = units.checked_mul(1_000_000).expect("Unit overflow");
		Self::from_microunits(microunits)
	}
}

impl<UnitName: UnitType> From<f64> for Unit<UnitName> {
	fn from(units: f64) -> Self {
		let microunits = num_traits::cast::NumCast::from(units.mul(1e6).round()).expect("Unit overflow");
		Self::from_microunits(microunits)
	}
}

impl<UnitName: UnitType> From<Unit<UnitName>> for f64 {
	fn from(unit: Unit<UnitName>) -> f64 {
		(unit.microunits as f64).div(1e6)
	}
}

impl<UnitName: UnitType> From<Unit<UnitName>> for Rational {
	fn from(unit: Unit<UnitName>) -> Rational {
		(unit.microunits as i128, 1_000_000).into()
	}
}

impl<UnitName: UnitType> From<Rational> for Unit<UnitName> {
	fn from(r: Rational) -> Self {
		Unit::from_microunits(r.mul(1_000_000).round().to_integer().try_into().unwrap())
	}
}

impl<UnitName: UnitType> Unit<UnitName> {
	pub fn cast<UnitName2: UnitType>(&self) -> Unit<UnitName2> {
		Unit::<UnitName2>::from_microunits(self.microunits)
	}

	pub fn from_f_floor(units: f64) -> Self {
		let microunits = num_traits::cast::NumCast::from(units.mul(1e6).floor()).expect("Unit overflow");
		Self::from_microunits(microunits)
	}

	pub fn from_f_ceil(units: f64) -> Self {
		let microunits = num_traits::cast::NumCast::from(units.mul(1e6).ceil()).expect("Unit overflow");
		Self::from_microunits(microunits)
	}

	pub fn from_r_floor(units: Rational) -> Self {
		let microunits = num_traits::cast::NumCast::from(units.mul(1_000_000).floor()).expect("Unit overflow");
		Self::from_microunits(microunits)
	}

	pub fn from_r_ceil(units: Rational) -> Self {
		let microunits = num_traits::cast::NumCast::from(units.mul(1_000_000).ceil()).expect("Unit overflow");
		Self::from_microunits(microunits)
	}
}

impl<UnitName: UnitType> std::fmt::Display for Unit<UnitName> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{} {}", self.quantity(), UnitName::UNIT_NAME)
	}
}

impl<UnitName: UnitType> SubAssign<Unit<UnitName>> for Unit<UnitName> {
	fn sub_assign(&mut self, rhs: Self) {
		*self = self.sub(rhs)
	}
}

impl<UnitName: UnitType> AddAssign<Unit<UnitName>> for Unit<UnitName> {
	fn add_assign(&mut self, rhs: Self) {
		*self = self.add(rhs)
	}
}

impl<UnitName: UnitType> Sub<Unit<UnitName>> for Unit<UnitName> {
	type Output = Self;
	fn sub(self, rhs: Self) -> Self::Output {
		Self::from_microunits(self.microunits.checked_sub(rhs.microunits).expect("unit overflow"))
	}
}

impl<UnitName: UnitType> Sub<&Unit<UnitName>> for Unit<UnitName> {
	type Output = Self;
	fn sub(self, rhs: &Unit<UnitName>) -> Self::Output {
		self.sub(*rhs)
	}
}

impl<UnitName: UnitType> Add<Unit<UnitName>> for Unit<UnitName> {
	type Output = Self;
	fn add(self, rhs: Self) -> Self::Output {
		Self::from_microunits(self.microunits.checked_add(rhs.microunits).expect("unit overflow"))
	}
}

impl<UnitName: UnitType> Add<&Unit<UnitName>> for Unit<UnitName> {
	type Output = Self;
	fn add(self, rhs: &Unit<UnitName>) -> Self::Output {
		self.add(*rhs)
	}
}

impl<UnitName: UnitType> Mul<i64> for Unit<UnitName> {
	type Output = Self;

	fn mul(self, rhs: i64) -> Self::Output {
		Self::from_microunits(self.microunits.checked_mul(rhs).expect("unit overflow"))
	}
}

impl<UnitName: UnitType> Mul<&i64> for Unit<UnitName> {
	type Output = Self;

	fn mul(self, rhs: &i64) -> Self::Output {
		self.mul(*rhs)
	}
}

impl<UnitName: UnitType> Mul<f64> for Unit<UnitName> {
	type Output = f64;

	fn mul(self, rhs: f64) -> Self::Output {
		f64::from(self).mul(rhs)
	}
}

impl<UnitName: UnitType> Mul<Rational> for Unit<UnitName> {
	type Output = Rational;

	fn mul(self, rhs: Rational) -> Self::Output {
		Rational::from(self).mul(rhs)
	}
}

impl<UnitName: UnitType> Mul<Unit<UnitName>> for Rational {
	type Output = Rational;

	fn mul(self, rhs: Unit<UnitName>) -> Self::Output {
		self.mul(Rational::from(rhs))
	}
}

impl<UnitName: UnitType> Mul<&f64> for Unit<UnitName> {
	type Output = f64;
	fn mul(self, rhs: &f64) -> Self::Output {
		self.mul(*rhs)
	}
}

impl<UnitName: UnitType> Div<Unit<UnitName>> for Rational {
	type Output = Rational;

	fn div(self, rhs: Unit<UnitName>) -> Self::Output {
		rhs.div(Rational::from(rhs))
	}
}

impl<UnitName: UnitType> Div<Rational> for Unit<UnitName> {
	type Output = Rational;

	fn div(self, rhs: Rational) -> Self::Output {
		Rational::from(self).div(rhs)
	}
}

impl<UnitName: UnitType> Div<f64> for Unit<UnitName> {
	type Output = f64;

	fn div(self, rhs: f64) -> Self::Output {
		f64::from(self).div(rhs)
	}
}

impl<UnitName: UnitType> Div<&f64> for Unit<UnitName> {
	type Output = f64;
	fn div(self, rhs: &f64) -> Self::Output {
		self.div(*rhs)
	}
}

impl<UnitName: UnitType> Div<i64> for Unit<UnitName> {
	type Output = Self;

	fn div(self, rhs: i64) -> Self::Output {
		Self::from_microunits(self.microunits.div(rhs))
	}
}

impl<UnitName: UnitType> Div<&i64> for Unit<UnitName> {
	type Output = Self;
	fn div(self, rhs: &i64) -> Self::Output {
		self.div(*rhs)
	}
}

impl<UnitName: UnitType> Div<Unit<UnitName>> for Unit<UnitName> {
	type Output = Rational;

	fn div(self, rhs: Unit<UnitName>) -> Self::Output {
		Rational::from((self.microunits as i128, rhs.microunits as i128))
	}
}
